package com.ndkien.demo_mongodb.repository;

import com.ndkien.demo_mongodb.model.Staff;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StaffRepository extends MongoRepository<Staff,String> {
}
