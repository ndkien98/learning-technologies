package com.ndkien.demo_mongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collation = "staff")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Staff {

    @Id
    private String id;

    private String code;
    private String name;
    private Long salary;


    public Staff(String code, String name, Long salary) {
        this.code = code;
        this.name = name;
        this.salary = salary;
    }
}
