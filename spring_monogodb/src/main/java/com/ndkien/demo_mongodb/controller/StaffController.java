package com.ndkien.demo_mongodb.controller;

import com.ndkien.demo_mongodb.model.Staff;
import com.ndkien.demo_mongodb.model.Tutorial;
import com.ndkien.demo_mongodb.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/staff/" +
        "")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @GetMapping("get-all")
    public ResponseEntity<List<Staff>> getAllTutorials() {

       return new ResponseEntity<>(staffRepository.findAll(),HttpStatus.OK);

    }

    @PostMapping("create-list")
    public ResponseEntity<List<Staff>> createListTutorial(@RequestBody Staff[] staffs) {

        List<Staff> list = new ArrayList<>();

        for (Staff staff:staffs
             ) {
            list.add(staffRepository.save(new Staff(staff.getCode(),staff.getName(),staff.getSalary())));
        }

        return new ResponseEntity<>(list, HttpStatus.CREATED);
    }

}
